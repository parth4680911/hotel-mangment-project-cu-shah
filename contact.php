<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php require('inc/links.php'); ?>
  <title>HOTEL CHATAKO- CONTACT</title>
</head>
<body class="bg-light">

  <?php require('inc/header.php'); ?>

  <div class="my-5 px-4">
    <h2 class="fw-bold h-font text-center">CONTACT US</h2>
    <div class="h-line bg-dark"></div>
    <p class="text-center mt-3">
    Hotel Chatako understand that every family needs a quality time well spent. Whether dining in or celebrating a special occasion, Hotel Chatako one provides unparalleled level of food, fervor and personal services to all its guests under one roof as they also enjoy its unique spirit of genuine hospitality in its true sense.
    </p>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 mb-5 px-4">

        <div class="bg-white rounded shadow p-4">
          <iframe class="w-100 rounded mb-4" height="320px" src="//www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3673.0214477171894!2d72.62488281496717!3d22.986238784971412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e8613f0000017%3A0x1abdc31e3b7d8b0b!2sHotel%20Chatako!5e0!3m2!1sen!2sin!4v1672231453134!5m2!1sen!2sin" loading="lazy"></iframe>

          <h5>Address</h5>
          <a href="<?php echo $contact_r['gmap'] ?>" target="_blank" class="d-inline-block text-decoration-none text-dark mb-2">
            <i class="bi bi-geo-alt-fill"></i> Nr.N.H., 8, Narol - Naroda Rd, C.T.M, Amraiwadi, Ahmedabad, Gujarat 380026
          </a>

          <h5 class="mt-4">Call us</h5>
          <a href="tel: 25890510" class="d-inline-block mb-2 text-decoration-none text-dark">
            <i class="bi bi-telephone-fill"></i> 25890510
          </a>
          <br>
          <?php 
            if($contact_r['pn2']!=''){
              echo<<<data
                <a href="tel: 25896539" class="d-inline-block text-decoration-none text-dark">
                  <i class="bi bi-telephone-fill"></i> 25896539
                </a>
              data;
            }
          ?>


          <h5 class="mt-4">Email</h5>
          <a href="mailto: info@chatakohotel.com" class="d-inline-block text-decoration-none text-dark">
            <i class="bi bi-envelope-fill"></i> info@chatakohotel.com
          </a>

          <h5 class="mt-4">Follow us</h5>
          <?php 
            if($contact_r['tw']!=''){
              echo<<<data
                <a href="https://twitter.com/hotelchatako?s=11&t=16zXr6Pi15_3OV5U2Z8N-A" class="d-inline-block text-dark fs-5 me-2">
                  <i class="bi bi-twitter me-1"></i>
                </a>
              data;
            }
          ?>

          <a href="https://www.facebook.com/hotelchatakoGJ?" class="d-inline-block text-dark fs-5 me-2">
            <i class="bi bi-facebook me-1"></i>
          </a>
          <a href="https://instagram.com/chatakohotel?igshid=YmMyMTA2M2Y=" class="d-inline-block text-dark fs-5">
            <i class="bi bi-instagram me-1"></i>
          </a>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 px-4">
        <div class="bg-white rounded shadow p-4">
          <form method="POST">
            <h5>Send a message</h5>
            <div class="mt-3">
              <label class="form-label" style="font-weight: 500;">Name</label>
              <input name="name" required type="text" class="form-control shadow-none">
            </div>
            <div class="mt-3">
              <label class="form-label" style="font-weight: 500;">Email</label>
              <input name="email" required type="email" class="form-control shadow-none">
            </div>
            <div class="mt-3">
              <label class="form-label" style="font-weight: 500;">Subject</label>
              <input name="subject" required type="text" class="form-control shadow-none">
            </div>
            <div class="mt-3">
              <label class="form-label" style="font-weight: 500;">Message</label>
              <textarea name="message" required class="form-control shadow-none" rows="5" style="resize: none;"></textarea>
            </div>
            <button type="submit" name="send" class="btn text-white custom-bg mt-3">SEND</button>
          </form>
        </div>
      </div>
    </div>
  </div>


  <?php 

    if(isset($_POST['send']))
    {
      $frm_data = filteration($_POST);

      $q = "INSERT INTO `user_queries`(`name`, `email`, `subject`, `message`) VALUES (?,?,?,?)";
      $values = [$frm_data['name'],$frm_data['email'],$frm_data['subject'],$frm_data['message']];

      $res = insert($q,$values,'ssss');
      if($res==1){
        alert('success','Mail sent!');
      }
      else{
        alert('error','Server Down! Try again later.');
      }
    }
  ?>

  <?php require('inc/footer.php'); ?>

</body>
</html>